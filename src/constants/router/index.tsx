import { createBrowserRouter } from 'react-router-dom';
import { PAGE_LINK } from '@/constants/links';
import { lazy, Suspense } from 'react';

const LazyHomePage = lazy(() => import('@/pages/home'));

export const router = createBrowserRouter([
  {
    path: PAGE_LINK.HOME,
    element: (
      <Suspense fallback={<div>Loading...</div>}>
        <LazyHomePage />
      </Suspense>
    ),
  },
  {
    path: '*',
    element: <div>No match page</div>,
  },
]);
