import { useAppSelector } from '@/hooks/useAppSelector';

const HomePage = () => {
  const auth = useAppSelector(state => state.auth);

  console.log(auth);

  return <div>HomePage</div>;
};

export default HomePage;
